# Springboot Micro Service Demo Repo

This repo's only function it to store configuration for all our micro-services. This repo is configured as a datasource for the Spring Cloud Config Server.
All configuration files for our estate can be managed from a single `consistent` point.

# Some rules that apply:

# Application.yaml Domain level configuration
This is the master settings file for the default profile for all services. Settings that are completely ubiquitous in nature can be centrally
managed from this file. Spring always loads the default profile and then will seek to override configuration based on a set of rules.

An example domain level setting would be:

```yaml
  axon:
  axonserver:
    servers: ${AXON_HOST:localhost}
  serializer:
    general: "jackson"
    events: "jackson"
    messages: "jackson"`
```

# Application-customProfile.yaml  (`Applicaiton-ProfileName.yaml`)  Profile specific configuration
This is for all services in the domain, however specifying a profile specific setting here will override what is specified in the 
Application.yaml file as an example in a testing profile we may want to change some semantics:

`Applicaiton-Staging.yaml`

```yaml
yobnk:
  logging:
    includeDockerInfo: ${DOCKER_INFO:false}
    includeK8Info: ${K8_INFO:false}
    logzIOFormat: ${LOGZ_IO_FORMAT:false}`
```


# GreetingsService.yaml  (`ServiceName.yaml`) Micro service specific configuration
These values override what is set in the Appicaiton.yaml when a particular micro-service with this name is
requesting settings. However should the applicaiton be running with a custom profile, then the custom profile overrides 
the value in this file. 

Example if Greetings service is running under the custom profile, and the key is provided in the Application-customProfile.yaml 
file then the value is sourced from the profile specific file.

# GreetingService-customProfile  (`ServiceName-ProfileName`) Micro-service / profile specific configuration
This configuration file will trump the above configuration as it is the most specific. 



